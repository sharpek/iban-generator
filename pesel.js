function getDigit() {
    return Math.floor(Math.random() * 10);
}
function getLetter (items)
{
    return items[Math.floor(Math.random() * items.length)];
}
function calcPesel(date, sex) {
    var wagi = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];

    var fullYear = date.getFullYear();
    var y = fullYear % 100;
    var m = date.getMonth() + 1;
    var d = date.getDate();
    var i;

    if (fullYear >= 1800 && fullYear <= 1899) {
        m += 80;
    } else if (fullYear >= 2000 && fullYear <= 2099) {
        m += 20;
    } else if (fullYear >= 2100 && fullYear <= 2199) {
        m += 40;
    } else if (fullYear >= 2200 && fullYear <= 2299) {
        m += 60;
    }

    var cyfry = [Math.floor(y / 10), y % 10, Math.floor(m / 10), m % 10, Math.floor(d / 10), d % 10];
    for (i = cyfry.length; i < wagi.length - 1; i++) {
        cyfry[i] = getDigit();
    }

    if (sex == 'M') {
        cyfry[wagi.length - 1] = getLetter('13579');
    } else if (sex == 'F') {
        cyfry[wagi.length - 1] = getLetter('02468');
    } else {
        cyfry[wagi.length - 1] = getDigit();
    }

    var cyfra_kontrolna = 0;
    for (i = 0; i < cyfry.length; i++) {
        cyfra_kontrolna += wagi[i] * cyfry[i];
    }
    cyfra_kontrolna = (10 - (cyfra_kontrolna % 10)) % 10;

    var result = '';
    for (i = 0; i < cyfry.length; i++) {
        result += "" + cyfry[i];
    }
    result += "" + cyfra_kontrolna;
    return result;
}
