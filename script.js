$(document).ready(function() {
    $('#iban-tab form').on('submit', function() {
        $('select').trigger('change');
        $('#iban-tab #iban')[0].focus();
        $('#iban-tab #iban')[0].select();
        return false;
    });

    $('body').on('change', '#iban-tab select', function() {
        var bank = $('#iban-tab #bank').val();
        if (bank.length === 4) {
            bank += ("" + Math.random() * 100000).substr(0, 4);
        }
        var country = CountryData($('#country').val());
        var account_number = parseInt(Math.random() * 99999999999999);
        account_number += "";
        account_number = account_number.substr(0, country.acc_lng)

        if (country.code === 'GB') {
            bank = 'BARC200415';
        }

        $('#iban-tab #iban').val(
            CalcIBAN(country, bank, account_number)
        );
    });
    $('#iban-tab select').trigger('change');
    $('#iban-tab #iban').on('click', function() {
        this.select();
    });
    $('#generate-pesel').on('click', function() {
        generatePesel();
        return false;
    });

    var generatePesel = function() {
        var sex = $('#sex').val()
        var date = $('#birthday').val();
        if (!date) {
            date = new Date();
        } else {
            date = new Date(Date.parse(date));
        }
        var pesel = calcPesel(date, sex);
        $('#pesel').val(pesel);
    }
    generatePesel();
});
